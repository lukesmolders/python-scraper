from json.encoder import JSONEncoder
from MarketPlaceItem import MarketPlaceItem
import json

class JsonHandler:
    def SaveToJSON(marketplaceList:list[MarketPlaceItem]):
        data = {}
        data['marketListing'] = []
        for item in marketplaceList:
            data['marketListing'].append({
                'price': item.Price,
                'title': item.Title,
                'href': item.Href,
                'imgUrl': item.ImgUrl
            })

        with open('listings.json', 'w') as outfile:
            json.dump(data, outfile, indent=4)

    def ReadFromJSON():
            with open('listings.json') as json_file:
                return json.load(json_file)

    def SearchInJSON(url:str, data):
        f = open('listings.json',)
        data = json.load(f)
        # Iterating through the json
        # list
        for i in data['marketListing']:
            print(i)

    

staticmethod(JsonHandler.SaveToJSON)
staticmethod(JsonHandler.ReadFromJSON)
staticmethod(JsonHandler.SearchInJSON)