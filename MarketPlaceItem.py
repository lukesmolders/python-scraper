from typing import Text


class MarketPlaceItem:
    Price:int
    Title: str
    Href: str
    ImgUrl: str

    def __init__(self, price:int, title:str, href:str, imgUrl:str):
        self.Price = price
        self.Title = title
        self.Href = href
        self.ImgUrl = imgUrl