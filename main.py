from typing import Text
import re
from MarketPlaceItem import MarketPlaceItem
from JsonHandler import JsonHandler
import asyncio
from pyppeteer import launch


async def main():
    browser = await launch()
    page = await browser.newPage()
    await page.goto('https://www.facebook.com/marketplace/112886808726198/search/?query=xbox%20series%20s')
    await asyncio.sleep(0.25)

    # Marketplace items
    marketplaceItems: MarketPlaceItem = []

    xPathQueryHrefAndTitle = await page.xpath('//a//img//parent::*/parent::*/parent::*/parent::*/parent::*/parent::*/parent::*/parent::*')
    xPathQueryImg = await page.xpath('//a//img')

    for item in xPathQueryHrefAndTitle:
        itemTitle = await page.evaluate('(element) => element.innerText', item)
        itemTitle = itemTitle.replace("\xa0", " ").replace("\n", " ").replace("\u00a0", " ").replace("\u20ac", " ")
        itemTitle = itemTitle[2:]
        listingPrice = re.findall(r"\d+", itemTitle)

        marketplaceItems.append(
            MarketPlaceItem(
                href= await page.evaluate('(element) => element.href', item),
                imgUrl="",
                title=itemTitle,
                price= listingPrice[0]
            ))

    listWithImages: Text = []
    for imageUrl in xPathQueryImg:
        listWithImages.append(await page.evaluate('(element) => element.currentSrc', imageUrl))

    for mkPlaceItem, image in zip(marketplaceItems, listWithImages):
        mkPlaceItem.ImgUrl = image

    await browser.close()

    JsonHandler.SaveToJSON(marketplaceList=marketplaceItems)
    # data = JsonHandler.ReadFromJSON;
    # JsonHandler.SearchInJSON("f", data)

asyncio.get_event_loop().run_until_complete(main())
